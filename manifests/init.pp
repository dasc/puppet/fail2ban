class fail2ban (
    Boolean $ssh_jail_enable,
    Boolean $sshpermaban_jail_enable,
    String $package_ensure,
    String $ssh_jail_name,
    String $ssh_filter,
    String $ssh_logpath,
    String $cron_report_ensure,
    String $sshpermaban_logpath,
    String $local_file_store = 'puppet://puppet/modules/fail2ban',
    Integer $bantime,
    String $destemail,
    String $sender,
    String $logtarget,
    Optional[String] $action = 'action_mwl',
    Array[String] $cron_report_users,
    Array[String] $ignore_ip,
    Enum['CRITICAL', 'ERROR', 'WARNING', 'NOTICE', 'INFO', 'DEBUG'] $logging_level,


) {
    package {'fail2ban': ensure => $package_ensure, }

    service {'fail2ban':
	ensure => 'running',
	enable => true,
	require => Package['fail2ban'],
    }

    file { '/etc/fail2ban/fail2ban.conf':
	ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('fail2ban/fail2ban.conf.epp', {
	    'logtarget' => $logtarget,
	    'logging_level' => $logging_level,
	}),
	require => Package['fail2ban'],
    }

    file { '/etc/fail2ban/jail.d/00-firewalld.conf':
	ensure => absent,
	require => Package['fail2ban'],
	notify => Service['fail2ban'],
    }

    file { '/etc/fail2ban/jail.d/sshd.conf':
	ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('fail2ban/sshd.conf.epp', {
	    'name' => $ssh_jail_name,
	    'enable' => $ssh_jail_enable,
	    'filter' => $ssh_filter,
	    'logpath' => $ssh_logpath,
	    'ignore_ip' => $ignore_ip,
	    'action' => $action,
	}),
	require => Package['fail2ban'],
	notify => Service['fail2ban'],
    }

    file { '/etc/fail2ban/jail.d/sshd-repeater.conf':
	ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('fail2ban/sshd-repeater.conf.epp', {
	    'name' => 'ssh-repeater',
	    'logpath' => $sshpermaban_logpath,
	    'destemail' => $destemail,
	    'filter' => $ssh_filter,
	    'sender' => $sender,
	    'enable' => $sshpermaban_jail_enable,
	    'ignore_ip' => $ignore_ip,
	}),
	require => Package['fail2ban'],
	notify => Service['fail2ban'],
    }

    file { '/etc/fail2ban/action.d/iptables-repeater.conf':
	ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0644',
	source => "${local_file_store}/etc/fail2ban/action.d/iptables-repeater.conf",
	require => Package['fail2ban'],
	notify => Service['fail2ban'],
    }

    file { '/etc/fail2ban/jail.d/jail.conf.local':
	ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('fail2ban/jail.conf.local.epp', {
	    'bantime' => $bantime,
	    'destemail' => $destemail,
	    'sender' => $sender,
	}),
	require => Package['fail2ban'],
	notify => Service['fail2ban'],
    }

    file { '/usr/bin/fail2ban_summarizer' :
	ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0755',
	source => [ "${local_file_store}/usr/bin/fail2ban_summarizer",],
    }

    $cron_email_recipients=join($cron_report_users, ',')
    cron {'fail2ban_summarizer_cron':
	ensure => $cron_report_ensure,
	command => '/usr/bin/fail2ban_summarizer',
	hour => 0,
	minute => 15,
	user => 'root',
	environment => "MAILTO=${cron_email_recipients}",
    }
}
